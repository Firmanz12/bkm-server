<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserRole;
use App\User;

class RoleController extends Controller
{
    //
    protected $page = "role";
    public function index()
    {
    	$role = UserRole::all();
    	$data['roles'] = $role;
    	$data['page'] = $this->page;
    	return view('dashboard.role.role', $data);
    }

    public function storeRole(Request $request)
    {
    	$validate = $request->validate([
    		'role_name' => 'required',
    		'role_value' => 'required|regex:/^[A-Za-z_]+$/',
    	]);
    	UserRole::create([
    		"role_name" => $request->input('role_name'),
    		"role_raw_value" => $request->input('role_value'),
    	]);
    	return redirect()->route('role-list');
    }
}
