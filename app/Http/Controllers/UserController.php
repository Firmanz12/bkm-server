<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Token;
use App\User;
use App\UserRole;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $page = 'user';
    protected $codeResponse = 200;
    protected $messageResponse = "success";
    
    public function index()
    {
        $user = User::getDataWithRole()->get();
        $data['users'] = $user;
        $data['page'] = $this->page;
        return view('dashboard.user.user', $data);
    }

    public function addUser()
    {
        // dd(UserRole::all());
        $data['roles'] = UserRole::all();
        $data['page'] = $this->page;
        return view('dashboard.user.add_user', $data);
    }

    public function storeUser(Request $request)
    {
        // dd($request->input('role'));
        $validate = $request->validate([
            'username' => 'required',
            'email' => 'required|string|max:225',
            'password' => 'required|min:8',
            'role' => 'required'
        ]);
        User::create([
            "id_role" => $request->input('role'),
            "name" => $request->input('username'),
            "email" => $request->input('email'),
            "password" => Hash::make($request->input('password'))
        ]);
        return redirect()->route('user-list');
    }

    public function getUserData(Request $request)
    {
        if ($request->get('with_email')) {
            $user = User::where('email', '!=', $request->get('email'))->get();
            if (!empty($user)) {
                $data = [
                    "code" => $this->codeResponse,
                    "msg" => $this->messageResponse,
                    "data" => $user
                ];
                return response()->json($data);
            }else{
                $this->codeResponse = 400;
                $this->messageResponse = "failed";
    
                $data = [
                    "code" => $this->codeResponse,
                    "msg" => $this->messageResponse,
                    "data" => []
                ];
                return response()->json($data);
            }
        }elseif ($request->get('with_token')) {
            try {
                $user_data = User::getDataWithToken($request->get('access_token'))->first();
                if (!empty($user_data)) {
                    $data = [
                        "code" => 200,
                        "msg" => "success",
                        "data" => $user_data
                    ];
                    return response()->json($data);
                }else{
                    $data = [
                        "code" => 400,
                        "msg" => "unauthorized",
                    ];
                    return response()->json($data);
                }
            } catch (Exception $e) {
                $data = [
                    "code" => 400,
                    "msg" => "error occured"
                ];
                return response()->json($data);
            }
        }elseif ($request->get('with_id')) {
            $user = User::where('id', '=', $request->get('id'))->first();
            if (!empty($user)) {
                $data = [
                    "code" => $this->codeResponse,
                    "msg" => $this->messageResponse,
                    "data" => $user
                ];
                return response()->json($data);
            }else{
                $this->codeResponse = 400;
                $this->messageResponse = "failed";
    
                $data = [
                    "code" => $this->codeResponse,
                    "msg" => $this->messageResponse,
                    "data" => []
                ];
                return response()->json($data);
        }
    }
}

    

    public function requestData(Request $request)
    {
        try {
            $user_data = User::getDataWithToken($request->get('access_token'))->first();
            if (!empty($user_data)) {
                $data = [
                    "code" => 200,
                    "msg" => "success",
                    "data" => $user_data
                ];
                return response()->json($data);
            }else{
                $data = [
                    "code" => 400,
                    "msg" => "unauthorized",
                ];
                return response()->json($data);
            }
        } catch (Exception $e) {
            $data = [
                "code" => 400,
                "msg" => "error occured"
            ];
            return response()->json($data);
        }
    }
}
