<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Token;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;

error_reporting(1);
session_start();
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        try {
            $access_token = $request->session()->get('access_token');
            $token = Token::where('access_token', $request->session()->get('access_token'))->first();
            if (!empty($token)) {
                $token->delete();
                $request->session()->forget('access_token');
                return redirect()->intended($request->get('continue'));
            }else{
                $request->session()->forget('access_token');
                return redirect()->intended($request->get('continue'));
            }
        } catch (Exception $e) {
            
        }

    }

    public function login(Request $request)
    {
        $validate = $request->validate([
            'email' => 'required|string|max:225',
            'password' => 'required',
            'continue_path' => 'required'
        ]);
        $email = $request->input('email');
        $continue_path = $request->input('continue_path');
        $user = User::where('email', $email)->first();
        $access_token = md5($user->email.$user->name);
        if (!empty($user) && Hash::check($request->input('password'),$user->password)) {
            $token_exist = Token::where("user_id", $user->id)->first();
            if (!empty($token_exist)) {
                $token_exist->access_token = $access_token;
                $token_exist->save();
                
                $request->session()->put('access_token', $access_token);
                $request->session()->save();
                
                if ($user->role == "admin") {
                    return redirect()->route('user-list');
                }
                return redirect()->intended($continue_path."?bkmt=".$access_token);
            }

            $token = new Token;
            $token->access_token = $access_token;
            $token->user_id = $user->id;
            $token->save();

            $request->session()->put('access_token', $access_token);
            $request->session()->save();

            if ($user->role == "admin") {
                return redirect()->route('user-list');
            }
            return redirect()->intended($continue_path."?bkmt=".$access_token);
        }else{
            $request->session()->flash("msg", "user not found, please check your email or password");
            return redirect()->back();
        }
    }

    public function check(Request $request)
    {
        try {
            $token = Token::where('access_token', $request->get('access_token'))->first();
            if (!empty($token)) {
                $data = [
                    "code" => 200,
                    "msg" => "success",
                    "token" => $token->access_token
                ];
                return response()->json($data);
            }else{
                $data = [
                    "code" => 400,
                    "msg" => "unauthorized",
                ];
                return response()->json($data);
            }
        } catch (Exception $e) {
            $data = [
                "code" => 400,
                "msg" => "unauthorized",
            ];
            return response()->json($data);
        }
    }
}
