<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Token;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->check()) {
        //     return redirect('/home');
        // }

        if (!empty($request->session()->get('access_token'))) {
            $token = Token::where('access_token',$request->session()->get('access_token'))->first();
            return redirect()->to($request->get('continue').'?gid='.$token->access_token);
        }

        return $next($request);
    }
}
