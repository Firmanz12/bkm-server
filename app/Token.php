<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    //
    public $table = 'token';
    protected $fillable = [
        'access_token', 'user_id'
    ];
}
