<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection = 'mysql';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', "id_role"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getDataWithToken($token)
    {
        if (!empty($token)) {
            $query = static::select('users.id', 'users.name', 'users.email', 'token.access_token', 'token.is_active')->join('token', 'token.user_id', '=', 'users.id')->where('token.access_token', $token);
            return $query;
        }
    }

    public static function getDataWithRole(){
        $query = static::select('users.id', 'users.name', 'users.email', 'users.created_at', 'user_role.role_name')->join('user_role', 'user_role.id', "=", 'users.id_role');
        return $query;
    }
}
