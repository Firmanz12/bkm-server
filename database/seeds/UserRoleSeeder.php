<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user = [
    		[
        		"role_name" => "admin",
        		"role_raw_value" => "admin",
        		"created_at" => Carbon::now(),
        		"updated_at" => Carbon::now()
        	],
        	[
        		"role_name" => "Sales Admin",
        		"role_raw_value" => "sales_admin",
        		"created_at" => Carbon::now(),
        		"updated_at" => Carbon::now()
        	],
        	[
        		"role_name" => "Customer Service",
        		"role_raw_value" => "customer_service",
        		"created_at" => Carbon::now(),
        		"updated_at" => Carbon::now()
        	]
    	];
        DB::table('user_role')->insert($user);
    }
}
