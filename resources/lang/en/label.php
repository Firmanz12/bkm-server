<?php

return [

    /*
    |--------------------------------------------------------------------------
    | layout
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'menu' => [
        'user-list' => 'Daftar Pengguna',
        'add-user' => 'Tambah User',
        'role-list' => 'Daftar Role',
        'logout' => 'Logout'
    ],

    'input' => [
        'user-name' => 'Username',
        'email' => 'Email',
        'role' => 'Role',
        'password' => "password",
        'placeholder' => [
            'email' => "Masukan email",
            'username' => "Masukan username",
            'password' => "Masukan Password",
            'role-name' => "Nama Role",
            'role-value' => "Value Role (contoh: example_example)",
        ],
        'role' => [
            'add-role' => 'Tambah Role',
            'role-name' => "Nama Role",
            'role-value' => "Value Role"
        ]
    ],

    'data' => [
        'title' => [
            'user-list' => "Daftar user",
            'role-list' => "Daftar role",
        ],
        'user' => [
            'email' => 'Email',
            'user-name' => 'Nama User',
            'user-type' => 'Tipe User',
            'register-date' => 'Tanggal Registrasi',
        ],
        'role' => [
            'role-name' => 'Nama Role',
            'role-value' => 'Value Role',
            'role-id' => 'Id Role',
        ],
        'action' => 'Aksi'
    ]
];
