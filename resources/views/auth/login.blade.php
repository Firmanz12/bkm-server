@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="col-md-12 header-container">
                    <div class="header-image">
                        <img src="{{ asset('img/bkm-logo.png') }}" alt="bkm-logo">
                    </div>
                    <p class="header-title">Welcome</p>
                </div>
                <div class="card-body" style="padding-top: 0px">
                    <form method="POST" action="{{ route('signin') }}">
                        @csrf
                        @if (session('msg'))
                            <div class="col-md-12">
                                <div class="msg-alert">
                                    <p>{{ session('msg') }}</p>
                                </div>
                            </div>
                        @endif
                        <div class="form-group row">
                            <div class="col-md-12 login-group">
                                <label for="email" class="input-label">Email</label>
                                <input type="hidden" value="<?php if(!empty($_GET['continue'])){echo $_GET['continue'];} ?>" name="continue_path">
                                <input type="email" class="input-textfield {{ $errors->first() ? 'alert' : '' }}" id="email" name="email" required placeholder="Email" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 login-group">
                                <label for="password" class="input-label">Password</label>
                                <input type="password" class="input-textfield" id="password" name="password" required placeholder="password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 login-group">
                            <button type="submit" class="login">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
