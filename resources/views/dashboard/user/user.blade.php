@extends('master.app')

@section('content')
<div class="main-wrapper">
	<div class="page-wrapper">
		<div class="row page-titles">
        	<div class="col-md-5 align-self-center">
        	    <h3 class="text-primary">User List</h3>
        	</div>
    	</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="card-content">
                                <h4 class="card-title">@lang('label.menu.user-list')</h4>
                                <div class="table-responsive m-t-40" style="margin-top: 20px!important">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>@lang('label.data.user.user-name')</th>
                                                <th>@lang('label.data.user.email')</th>
                                                <th>@lang('label.data.user.user-type')</th>
                                                <th>@lang('label.data.user.register-date')</th>
                                                <th>@lang('label.data.action')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1; foreach ($users as $user): ?>
                                            	<tr>
                                                	<td>{{ $i }}</td>
                                                	<td>{{ $user->name }}</td>
                                                	<td>{{ $user->email }}</td>
                                                	<td>{{ $user->role_name }}</td>
                                                	<td>{{ date('Y-m-d', strtotime($user->created_at)) }}</td>
                                                	<td>
                                                		<button class="btn btn-primary" value="{{ $user->id }}"><i class="fa fa-pencil"></i></button>
                                                		<button class="btn btn-danger" value="{{ $user->id }}"><i class="fa fa-trash"></i></button>
                                                	</td>
                                            	</tr>
                                            <?php $i++; endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection