@extends('master.app')

@section('content')
<div class="main-wrapper">
	<div class="page-wrapper">
		<div class="row page-titles">
    	    <div class="col-md-5 align-self-center">
    	        <h3 class="text-primary">Tambah User</h3>
    	    </div>
    	</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="card-content">
								<div class="form-validation">
                                    <form class="form-valide" action="{{ route('store-user') }}" method="post">
                                    	{{ csrf_field() }}
                                    	@if ($errors->has('username'))
                                    		<div class="form-group row is-invalid">
                                    	@else
                                    		<div class="form-group row">
                                    	@endif
                                            <label class="col-lg-2 col-form-label" for="username">Username <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                @if ($errors->has('username'))
                                                	<input type="text" class="form-control" id="username" name="username" placeholder="@lang('label.input.placeholder.username')" aria-invalid="true">
                                                	<div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('username') }}</div>
                                                @else
                                                	<input type="text" class="form-control" id="username" name="username" placeholder="@lang('label.input.placeholder.username')" aria-invalid="false" value="{{ old('username') }}">
                                                @endif
                                            </div>
                                        </div>
                                        @if ($errors->has('email'))
                                    		<div class="form-group row is-invalid">
                                    	@else
                                    		<div class="form-group row">
                                    	@endif
                                            <label class="col-lg-2 col-form-label" for="email">Email <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                @if ($errors->has('email'))
                                                	<input type="text" class="form-control" id="email" name="email" placeholder="@lang('label.input.placeholder.email')" aria-invalid="true">
                                                	<div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('email') }}</div>
                                                @else
                                                	<input type="text" class="form-control" id="email" name="email" placeholder="@lang('label.input.placeholder.email')" aria-invalid="false" value="{{ old('email') }}">
                                                @endif
                                            </div>
                                        </div>
                                        @if ($errors->has('role'))
                                    		<div class="form-group row is-invalid">
                                    	@else
                                    		<div class="form-group row">
                                    	@endif
                                            <label class="col-lg-2 col-form-label" for="val-role">Role <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                            	@if ($errors->has('role'))
                                                <select class="form-control" id="role" name="role" aria-invalid=true>
                                                	<option value="">--Pilih Role--</option>
                                                	<?php foreach ($roles as $role): ?>
                                                		<option value="{{ $role->id }}">{{ $role->role_name }}</option>
                                                	<?php endforeach ?>
                                                </select>
                                                <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('role') }}</div>
                                                @else
                                                <select class="form-control" id="role" name="role" aria-invalid=true>
                                                	<option value="">--Pilih Role--</option>
                                                	<?php foreach ($roles as $role): ?>
                                                		<?php if (old('role') == $role->id): ?>
                                                			<option value="{{ $role->id }}" selected="true">{{ $role->role_name }}</option>
                                                		<?php else: ?>
                                                			<option value="{{ $role->id }}">{{ $role->role_name }}</option>
                                                		<?php endif ?>
                                                	<?php endforeach ?>
                                                </select>
                                                @endif
                                            </div>
                                        </div>
                                        @if ($errors->has('password'))
                                    		<div class="form-group row is-invalid">
                                    	@else
                                    		<div class="form-group row">
                                    	@endif
                                            <label class="col-lg-2 col-form-label" for="password">Password <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                            	@if ($errors->has('password'))
                                                	<input type="password" class="form-control" id="password" name="password" placeholder="@lang('label.input.placeholder.password')" aria-invalid="true">
                                                	<div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('password') }}</div>
                                                @else
                                                	<input type="password" class="form-control" id="password" name="password" placeholder="@lang('label.input.placeholder.password')">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection