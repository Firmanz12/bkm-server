@extends('master.app')

@section('content')
<div class="main-wrapper">
	<div class="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-content">
							<h4 class="card-title">@lang('label.menu.role-list')</h4>
							<button class="btn btn-primary" id="btn-add-role" data-target="#add_role_modal" data-toggle="modal"><i class="fa fa-plus"></i> @lang('label.input.role.add-role')</button>
							<div class="table-responsive m-t-40" style="margin-top: 20px!important">
								<table id="myTable" class="table table-bordered table-striped">
									<thead>
                                        <tr>
                                        	<th>No</th>
                                            <th>@lang('label.data.role.role-id')</th>
                                            <th>@lang('label.data.role.role-name')</th>
                                            <th>@lang('label.data.role.role-value')</th>
                                            <th>@lang('label.data.action')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; foreach ($roles as $role): ?>
                                        	<tr>
                                            	<td>{{ $i }}</td>
                                            	<td>{{ $role->id}}</td>
                                            	<td>{{ $role->role_name }}</td>
                                            	<td>{{ $role->role_raw_value }}</td>
                                            	<td>
                                            		<button class="btn btn-primary" value="{{ $role->id }}"><i class="fa fa-pencil"></i></button>
                                            		<button class="btn btn-danger" value="{{ $role->id }}"><i class="fa fa-trash"></i></button>
                                            	</td>
                                        	</tr>
                                        <?php $i++; endforeach ?>
                                    </tbody>
                                </table>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalTitle" id="add_role_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalTitle">@lang('label.input.role.add-role')</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form method="POST" action="{{ route('add-role') }}" enctype="multipart/form-data">
      {{csrf_field()}}
      	<div class="modal-body">
        	<div class="box-body">
				@if ($errors->has('role_name'))
               		<div class="form-group is-invalid">
               	@else
               		<div class="form-group">
               	@endif
					<label for="judul_event">@lang('label.input.role.role-name')</label>
					@if ($errors->has('role_name'))
						<input type="text" name="role_name" class="form-control" id="role-name" placeholder="@lang('label.input.placeholder.role-name')" aria-invalid="true">
						<div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('role_name') }}</div>
					@else
						<input type="text" name="role_name" class="form-control" id="role-name" placeholder="@lang('label.input.placeholder.role-name')" value="{{ old('role_name') }}">
					@endif

				</div>
				
				@if ($errors->has('role_value'))
               		<div class="form-group is-invalid">
               	@else
               		<div class="form-group">
               	@endif
					<label for="role_value">@lang('label.input.role.role-value')</label>
					@if ($errors->has('role_value'))
						<input type="text" name="role_value" class="form-control" id="role-value" placeholder="@lang('label.input.placeholder.role-value')" aria-invalid="true">
						<div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('role_value') }}</div>
					@else
						<input type="text" name="role_value" class="form-control" id="role-value" placeholder="@lang('label.input.placeholder.role-value')" value="{{ old('role_value') }}">
					@endif
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection