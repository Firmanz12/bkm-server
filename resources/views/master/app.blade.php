<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>BKM SERVER</title>

    <!-- Styles -->
    <link href="{{ asset('dashboard/css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->

    <link href="{{ asset('dashboard/css/lib/calendar2/semantic.ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/lib/calendar2/pignose.calendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/select2/css/select2.css') }}" rel="stylesheet">

</head>
</head>
<body class="fix-header fix-sidebar">
    <div class="header">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!-- Logo -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">
                    <!-- Logo icon -->
                    <b><img src="{{ asset('img/bkm-logo.png') }}" alt="homepage" class="dark-logo" style="width: 100%;padding: 15px" /></b>
                    <!--End Logo icon -->
                </a>
            </div>
            <!-- End Logo -->
        </nav>
    </div>
    <div class="left-sidebar">
           <!-- Sidebar scroll-->
           <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <div class="card" style="box-shadow: none;">
                    <div class="row">
                        <div class="col-md-12" style="padding-right: 0px!important;padding-left: 0px!important">
                            <div class="card-body">
                                <div class="card-content">
                                    <div class="inbox-leftbar">
                                        <a class="btn btn-danger btn-block waves-effect waves-light" href="{{ route('add-user') }}">Tambah User</a>
                                        <div class="mail-list mt-4">
                                            <a class="list-group-item border-0 <?php if($page == "user"){echo 'text-danger';}else{echo '';} ?>" href="{{ route('user-list') }}">
                                                <i class="fa fa-user font-18 align-middle mr-2"></i>
                                                <?php if($page == "user") : ?>
                                                    <b>@lang('label.data.title.user-list')</b>
                                                <?php else : ?>
                                                    @lang('label.data.title.user-list')
                                                <?php endif ?>
                                            </a>
                                            <a class="list-group-item border-0 <?php if($page == "role"){echo 'text-danger';}else{echo '';} ?>" href="{{ route('role-list') }}">
                                                <i class="fa fa-archive font-18 align-middle mr-2"></i>
                                                <?php if($page == "role") : ?>
                                                    <b>@lang('label.data.title.role-list')</b>
                                                <?php else : ?>
                                                    @lang('label.data.title.role-list')
                                                <?php endif ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
               <!-- End Sidebar navigation -->
           </div>
           <!-- End Sidebar scroll-->
       </div>
    @yield('content')
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="{{ asset('dashboard/js/lib/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('dashboard/js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('dashboard/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('dashboard/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('dashboard/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('dashboard/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>

    <script src="{{ asset('dashboard/js/scripts.js') }}"></script>

    <script src="{{ asset('/dashboard/js/lib/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/datatables-init.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            <?php if ($errors->has('role_name') || $errors->has('role_value')): ?>
                $('#add_role_modal').modal('show');
            <?php endif ?>
        });
    </script>
</body>
</html>