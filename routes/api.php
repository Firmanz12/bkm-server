<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(["middleware" => ['api']], function(){
	Route::get('auth/session', 'Auth\LoginController@check');
	Route::group(["prefix" => "user"], function(){
		Route::get('/request-data', 'UserController@getUserData');
		Route::get('/request-data-with-token', 'UserController@requestData');
	});
});



