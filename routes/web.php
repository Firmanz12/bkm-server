<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['prefix' => 'dashboard'], function(){
	Route::get('/home', 'HomeController@index')->name('home');
	Route::group(['prefix' => 'user'], function(){
		Route::get('/', 'UserController@index')->name('user-list');
		Route::get('/add', 'UserController@addUser')->name('add-user');
		Route::post('/add', 'UserController@storeUser')->name('store-user');
	});
	Route::group(["prefix" => "role"], function(){
		Route::get('/', "RoleController@index")->name('role-list');
		Route::post('/add', 'RoleController@storeRole')->name('add-role');
	});
});
Route::get('auth/session', 'Auth\LoginController@check');
Route::post('/login', 'Auth\LoginController@login')->name('signin');
Route::get('/logout', 'Auth\LoginController@logout')->name('signout');

